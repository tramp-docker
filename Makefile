EMACS?=	emacs

ELCS=	tramp-docker.elc

.PHONY:	all clean

all:	$(ELCS)

clean:
	rm -f $(ELCS)

%.elc:	%.el
	$(EMACS) -Q --batch -f batch-byte-compile $<
